import MeasureLabel from './MeasureLabel.js';

export default {
    components: {
        Ml: MeasureLabel,
    },

    template: `
        <g>
            <rect x="0" y="0" width="900" :height="AMax" class="svgShape" />
            <line x1="0" x2="900" :y1="fdt+HallLength" :y2="fdt+HallLength" class="svgShape" />
            <rect :x="sc / 2 - 2" :y="DitchPos" width="2" height="25" class="svgShape" />
            <rect :x="DPos - 2" :y="AMax - 31" width="2" height="25" class="svgShape" />
            <g v-if="se > 0">
                <line x1="0" x2="900" :y1="AMax-se+HallLength" :y2="AMax-se+HallLength" class="svgShape" />
            </g>
            <line v-if="frame_shape == 'HA'" x1="0" x2="900" :y1="12" :y2="12" class="svgShape" />

            <Ml
                myID="veritcal-c" 
                :length="sc / 2" 
                :label="sc"
                :position=[0,0,0]
                :flip="flip">
            </Ml>

            <Ml
                myID="veritcal-d" 
                :length="DPos" 
                :label="se > 0 ? sc : sd"
                :position=[0,(AMax+30),0]
                :flip="flip">
            </Ml>

            <Ml
                myID="height"
                length=900
                :label="height"
                :position=[0,(AMax+20),0]
                :flip="flip">
            </Ml>
        </g>
    `,

    props: {
        frame_shape: {
            type: String,
            default: ''
        },
        sa: {
            type: Number,
            default: 0
        },
        sc: {
            type: Number,
            default: 0
        },
        sd: {
            type: Number,
            default: 0
        },
        se: {
            type: Number,
            default: 0
        },
        fdt: {
            type: Number,
            default: 0
        },
        height: {
            type: Number,
            default: 0
        },
        flip: {
            type: Boolean,
            default: false
        },
    },

    computed: {
        AMax() {
            if (this.sa > 150) {
                return 150;
            }
            return this.sa;
        },
        DPos() {
            if (this.se > 0 ) {
                return this.sc / 2;
            }
            return this.sd / 2;
        },
        DitchPos() {
            return this.frame_shape == 'HA' ? (this.fdt)/2 - 0.5: 12.5;
        },
        HallLength() {
            if (this.frame_shape.includes('H')) {
                return 12;
            }
            return 0;
        },
    }
}