import MeasureLabel from './MeasureLabel.js';

export default {
    components: {
        Ml: MeasureLabel,
    },

    template:`
        <g :transform="'translate('+((DoorHeight/2)*900/height+sc/2+5)+','+fdt/2+')'">
            <path d="M -6 13.5 A 13.5 13.5 0 0 1 -6 -13.5 L 6 -13.5 A 13.5 13.5 0 0 1 6 13.5Z" class="svgShape"/>
            <text transform="translate(36,0) scale(1,-1)" class="svgText">𝜙27x35</text>
        </g>
        <Ml v-if="hinge != ''"
            myID="hinge-depth"
            :length="(DoorHeight/2)*900/height"
            :label="DoorHeight/2"
            :position=[sc/2+5,0,0]
            :flip="flip">
        </Ml>
        <Ml v-if="depth != 0"
            myID="hinge-depth"
            :length="DepthLength"
            :label="depth"
            :position=[900-DepthLength,0,0]
            :flip="flip">
        </Ml>
        <Ml v-if="gap != 0"
            myID="hinge-gap"
            :length="gap"
            :label="gap"
            :position=[900-DepthLength-gap,0,0]
            :flip="flip">
        </Ml>
        <Ml v-if="hinge != ''"
            :length="5"
            :label="5"
            :position=[sc/2,0,0]
            :flip="flip">
        </Ml>
    `,

    props: {
        frame_type: {
            type: String,
            default: ''
        },
        hinge: {
            type: String,
            default: ''
        },
        sc: {
            type: Number,
            default: 0,
        },
        fdt: {
            type: Number,
            default: 0,
        },
        gap: {
            type: Number,
            default: 0,
        },
        depth: {
            type: Number,
            default: 0,
        },
        height: {
            type: Number,
            default: 0,
        },
        flip: {
            type: Boolean,
            default: false
        }
    },

    computed: {
        DepthLength() {
            if (this.frame_type == "四面框") {
                return this.depth / 2;
            }
            return this.depth;
        },
        
        DoorHeight() {
            return this.height - this.sc - 5 - this.gap - this.depth;
        }
    }

}