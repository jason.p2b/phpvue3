// 平頭鎖/單點鎖
import HoleRectangle from './HoleRectangle.js';
import HoleIngrained from './HoleIngrained.js';
import MeasureLabel from './MeasureLabel.js';

export default {
    components: {
        Ml: MeasureLabel,
        HRect: HoleRectangle,
        HIngrained: HoleIngrained
    },

    template: `
        <g v-if="lock_method == 'surface'">
            <g  :transform="'translate('+midX+','+yPos+')'">
                <HRect
                height="20"
                width="40"
                :ptY="midPos"
            ></HRect>
            </g>
            <Ml 
                :length="midPos"
                :label="midPos"
                :position=[midX-20,midPos+yPos,-90]>
            </Ml>
        </g>

        <g v-if="lock_method == 'ingrained'">
        <g  :transform="'translate('+midX+','+yPos+')'">
                <HIngrained
                height="29"
                width="71"
                :ptY="midPos"
            ></HIngrained>
            </g>
            <Ml 
                :length="midPos"
                :label="midPos"
                :position=[(midX+10),midPos+yPos,-90]>
            </Ml>
        </g>
    `,

    props: {
        midX: {
            type: Number,
            default: 0
        },
        yPos: {
            type: Number,
            default: 0
        },
        midPos: {
            type: Number,
            default: 0
        },
        lock_method: {
            type: String,
            default: 0
        }
    },
}