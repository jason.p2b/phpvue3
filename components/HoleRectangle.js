import MeasureLabel from './MeasureLabel.js';

export default {
    components: {
        Ml: MeasureLabel
    },

    template: `
        <rect :x="-width/2" :y="ptY-height/2" :height="height" :width="width" class="svgShape" />
        <Ml 
            :length="width"
            :label="width"
            :position=[-width/2,ptY+height/1+10,0]>
        </Ml>
        <Ml 
            :length="height"
            :label="height"
            :position=[width,ptY+height/2,-90]>
        </Ml>
    `,

    props: {
        height: {
            type: Number,
            default: 0
        },
        width: {
            type: Number,
            default: 0
        },
        ptY: {
            type: Number,
            default: 0
        }
    }
}
