import MeasureLabel from './MeasureLabel.js';
import LockBox from './LockBox.js';
import LockFlatHead from './LockFlatHead.js';
import LockHorizontal from './LockHorizontal.js';
import Lock26K from './Lock26K.js';
import LockJ5115 from './LockJ5115.js';
import LockCN607 from './LockCN607.js';

export default {
    components: {
        Ml: MeasureLabel,
        LBox: LockBox,
        LFlathead: LockFlatHead,
        LHorizontal: LockHorizontal,
        L26K: Lock26K,
        LJ5115: LockJ5115,
        LCN607: LockCN607
    },

    template: `
        <g v-if="!(lock_method == 'none' || lock == '')">
            <Ml
                length=5
                label="5"
                :position=[(sc/2),0,0]>
            </Ml>
            <Ml v-if="depth != 0"
                myID="lock-depth"
                :length="DepthLength"
                :label="depth"
                :position=[900-DepthLength,0,0]>
            </Ml>
            <Ml v-if="gap != 0"
                myID="lock-gap"
                :length="gap"
                :label="gap"
                :position=[900-DepthLength-gap,0,0]>
            </Ml>
            <Ml 
                myID="top-lock-height"
                :length="topLockLabel * 900 / height"
                :label="topLockLabel"
                :position=[(sc/2)+5,0,0]>
            </Ml>
            <Ml 
                myID="bottom-lock-height"
                :length="900 - sc/2 - 5 - (topLockLabel * 900 / height) - gap - DepthLength"
                :label="bottomLockLabel"
                :position=[bottomLockX,0,0]
                level=1>
            </Ml>

            <LBox v-if="lock_method == 'box'"
                :midX="bottomLockX"
                :yPos="HallLength"
                :midPos="LockMidPos"
            ></LBox>

            <LCN607 v-if="HoleType == 'CN607'"
                :midX="bottomLockX"
                :yPos="HallLength"
                :midPos="LockMidPos"
                :lock_method="lock_method"
            ></LCN607>
            <LFlathead v-if="HoleType == 'oneHole'"
                :midX="bottomLockX"
                :yPos="HallLength"
                :midPos="LockMidPos"
                :lock_method="lock_method"
            ></LFlathead>
            
            <LHorizontal v-if="(HoleType == 'twoHoles' || HoleType == 'power')"
                :midX="bottomLockX"
                :yPos="HallLength"
                :midPos="LockMidPos"
                :lock_method="lock_method"
                :type="HoleType"
            ></LHorizontal>

            <L26K v-if="HoleType == 'CN26K'"
                :midX="bottomLockX"
                :yPos="HallLength"
                :midPos="LockMidPos"
                :lock_method="lock_method"
            ></L26K>

            <LJ5115 v-if="HoleType == 'J5115'"
                :midX="bottomLockX"
                :yPos="HallLength"
                :midPos="LockMidPos"
                :lock_method="lock_method"
            ></LJ5115>

        </g>
    `,

    props: {
        lock: {
            type: String,
            default: ''
        },
        lock_height: {
            type: Number,
            default: 0,
        },
        lock_method: {
            type: String,
            default: ''
        },
        lock_height_origin: {
            type: String,
            default: ''
        },
        sc: {
            type: Number,
            default: 0,
        },
        fdt: {
            type: Number,
            default: 0,
        },
        gap: {
            type: Number,
            default: 0,
        },
        depth: {
            type: Number,
            default: 0,
        },
        height: {
            type: Number,
            default: 0,
        },
        // 60A, 4面框, etc
        frame_type: {
            type: String,
            default: ''
        },
        frame_shape: {
            type: String,
            default: ''
        },
        exterior_panel: {
            type: Number,
            default: 0
        }
    },

    computed: {
        DepthLength() {
            if (this.frame_type == "四面框") {
                return this.depth / 2;
            }
            return this.depth;
        },

        topLockLabel() {
            let pt = 0;
            
            if (this.lock_height_origin == "top") {
                pt = this.lock_height;
            } else if (this.lock_height_origin == "bottom") {
                pt = this.height - this.lock_height - this.gap - this.depth - this.sc - 5;
            } else if (this.lock_height_origin == "floor") {
                pt = this.height - this.lock_height - this.depth - this.sc - 5;
            }

            return pt;
        },

        bottomLockLabel() {
            let pt = 0;
            
            if (this.lock_height_origin == "top") {
                pt = this.height - this.sc - 5 - this.lock_height - this.gap - this.depth;
            } else if (this.lock_height_origin == "bottom") {
                pt = this.lock_height;
            } else if (this.lock_height_origin == "floor") {
                pt = this.lock_height - this.gap;
            }

            return pt;
        },
        
        bottomLockX() {
            return (this.topLockLabel  * 900 / this.height) + this.sc/2 + 5 ;
        },

        HoleType() {
            let pushBar = ['CN-1600','F6038','916A'];
            let oneHole = ['DA61Q','Power90'];
            let twoHoles = ['CN-21-01S', 'LF603T+DA61Y'];
            let twoHolesPower = ['9014'];
            let CN607 = ['CN-607','CN-608'];
            let CN26K = ['CN-26K'];
            let J5115 = ['J5115'];
            
            if (pushBar.includes(this.lock)) {
                return "";
            } else if (oneHole.includes(this.lock)) {
                return "oneHole";
            } else if (twoHoles.includes(this.lock)) {
                return "twoHoles";
            } else if (twoHolesPower.includes(this.lock)) {
                return "power";
            } else if (CN26K.includes(this.lock)) {
                return "CN26K";
            } else if (J5115.includes(this.lock)) {
                return "J5115";
            } else if (CN607.includes(this.lock)) {
                return "CN607";
            }

            return this.lock;
        },

        HallLength() {
            if (this.frame_shape.includes('H')) {
                return 12;
            }
            return 0;
        },
        LockMidPos() {
            if (this.frame_shape == "HA") {
                return this.fdt - this.exterior_panel - 31.5;
            }
            return (this.fdt - 1) / 2;
        }

    }
}