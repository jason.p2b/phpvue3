// Power J5115 框 開孔
// 表面式未完成

import HoleRectangle from './HoleRectangle.js';
import HoleIngrained from './HoleIngrained.js';
import MeasureLabel from './MeasureLabel.js';

export default {
    components: {
        Ml: MeasureLabel,
        HRect: HoleRectangle,
        HIngrained: HoleIngrained
    },

    template: `
        <g v-if="lock_method == 'surface'">
            <g  :transform="'translate('+(midX)+','+(midPos+yPos)+')'">
                <text>開孔 未設定</text>
            </g>
        </g>

        <g v-if="lock_method == 'ingrained'">
            <g  :transform="'translate('+(midX-35.5)+','+yPos+')'">
                <HIngrained
                    height="24"
                    width="111"
                    :ptY="midPos"
                    type="power"
                ></HIngrained>
            </g>
            <Ml 
                length="20"
                label="20"
                :position=[midX,midPos+yPos-12,0]>
            </Ml>

        </g>
    `,

    props: {
        midX: {
            type: Number,
            default: 0
        },
        yPos: {
            type: Number,
            default: 0
        },
        midPos: {
            type: Number,
            default: 0
        },
        lock_method: {
            type: String,
            default: 0
        }
    },
}