import MeasureLabel from './MeasureLabel.js';

export default {
    components: {
        Ml: MeasureLabel
    },

    template: `
        <polyline :points="FrontShape" style="stroke:#000; fill:none;" />
        <path :d="DitchShape" style="stroke:#000; fill:none;" />
        <polyline :points="BackShape" style="stroke:#000; fill:none;" />

        <Ml 
            myID="measure-a" 
            :length="AMax" 
            :label="sa" 
            :position=[0,AMax,-90]
            level=2
            :flip="flip" >
        </Ml>
        <Ml
            myID="measure-b" 
            :length="sb" 
            :label="sb" 
            :position=[0,sb,-90]
            :flip="flip">
        </Ml>
        <Ml
            myID="measure-c" 
            :length="sc" 
            :label="sc"
            :position=[0,0,0]
            :flip="flip">
        </Ml>
        <Ml
            myID="measure-d" 
            :length="sd" 
            :label="sd"
            :position=[0,(AMax+20),0]
            :flip="flip">
        </Ml>
        <Ml v-show="shape.includes('H')"
            myID="" 
            :length="12" 
            :label="12"
            :position=[sd,0,90]
            :flip="flip">
        </Ml>
        <Ml v-show="shape.includes('H')"
            myID="" 
            :length="19" 
            :label="19"
            :position=[0,30,0]
            :flip="flip">
        </Ml>
        <Ml
            myID="measure-fdt" 
            :length="fdt" 
            :label="fdt"
            :position=[sd,HallLength,90]
            :flip="flip">
        </Ml>
        <Ml
            myID="measure-less-fdt" 
            :length="AMax - fdt - se" 
            :label="sa - fdt - se - HallLength"
            :position=[sd,fdt+HallLength,90]
            :flip="flip">
        </Ml>
        <Ml v-show="se > 0"
            myID="measure-e" 
            :length="se-HallLength" 
            :label="se"
            :position=[sd,AMax-se+HallLength,90]
            :flip="flip">
        </Ml>
    `,
  
    props: {
        sa: {
            type: Number,
            default: 0
        },
        sb: {
            type: Number,
            default: 0
        },
        sc: {
            type: Number,
            default: 0
        },
        sd: {
            type: Number,
            default: 0
        },
        se: {
            type: Number,
            default: 0
        },
        fdt: {
            type: Number,
            default: 0
        },
        shape: {
            type: String,
            default: ''
        },
        flip: {
            type: Boolean,
            default: false
        },
    },

    computed: {
        AMax() {
            if (this.sa > 150) {
                return 150;
            }
            return this.sa
        },
        HallLength() {
            if (this.shape.includes('H')) {
                return 12;
            }
            return 0;
        },
        FrontShape() {
            if (this.shape.includes('H')) {
                return  (this.sc-2)+","+(this.fdt+14) + " " +
                        (this.sc-2)+",14 "+
                        "17,14 " +
                        "17,2 " +
                        "2,2 " +
                        "2,"+this.sb+" "+
                        "0,"+this.sb+
                        " 0,0 "+
                        " 19,0 " +
                        " 19,12 " +
                        this.sc+",12 "+
                        this.sc+","+(this.fdt+12)+" ";
            }

            return (this.sc-2)+","+(this.fdt+2) + " " +
                    (this.sc-2)+",2 "+
                    "2,2 " +
                    "2,"+this.sb+" "+
                    "0,"+this.sb+
                    " 0,0 "+
                    this.sc+",0 "+
                    this.sc+","+(this.fdt)+" ";
        },
        DitchShape() {
            let frame = "";
            
            // 無氣密
            if (this.shape.includes('I')) {
                frame = "M"+this.sc+" "+this.fdt+" "+
                        "L"+this.sd+" "+this.fdt+" "+
                        "M"+(this.sc-2)+" "+(this.fdt+2)+" "+
                        "L"+(this.sd-2)+" "+(this.fdt+2)+" ";
            }

            // 氣密
            if (this.shape.includes('A')) {
                frame = "M"+this.sc+" "+(this.fdt+this.HallLength)+" "+
                        "L"+this.sc+" "+(this.fdt+this.HallLength+13)+" "+
                        "L"+(this.sc+10)+" "+(this.fdt+this.HallLength+13)+" "+
                        "L"+(this.sc+10)+" "+(this.fdt+this.HallLength)+" "+
                        "L"+this.sd+" "+(this.fdt+this.HallLength)+" "+
                        "M"+(this.sc-2)+" "+(this.fdt+this.HallLength+2)+" "+
                        "L"+(this.sc-2)+" "+(this.fdt+this.HallLength+15)+" "+
                        "L"+(this.sc+12)+" "+(this.fdt+this.HallLength+15)+" "+
                        "L"+(this.sc+12)+" "+(this.fdt+this.HallLength+2)+" "+
                        "L"+(this.sd-2)+" "+(this.fdt+this.HallLength+2)+" ";
            }

            // 壓扁氣密
            if (this.shape.includes('F')) {
                frame = "M"+this.sc+" "+this.fdt+" "+
                        "L"+(this.sd-14)+" "+this.fdt+" "+
                        "L"+(this.sd-14)+" "+(this.fdt+13)+" "+
                        "L"+(this.sd-4)+" "+(this.fdt+13)+" "+
                        "L"+(this.sd-4)+" "+this.fdt+" "+
                        "L"+this.sd+" "+this.fdt+" "+
                        "M"+(this.sc-2)+" "+(this.fdt+2)+" "+
                        "L"+(this.sd-16)+" "+(this.fdt+2)+" "+
                        "L"+(this.sd-16)+" "+(this.fdt+15)+" "+
                        "L"+(this.sd-2)+" "+(this.fdt+15)+" "+
                        "L"+(this.sd-2)+" "+(this.fdt+2)+" ";
            }
            
            return frame;
        },
        BackShape() {
            return this.sd+","+(this.fdt+this.HallLength)+" "+
                    this.sd+","+(this.AMax-this.se+this.HallLength)+" "+
                    this.sc+","+(this.AMax-this.se+this.HallLength)+" "+
                    this.sc+","+this.AMax+" "+
                    "0,"+this.AMax+" "+
                    "0,"+(this.AMax-this.sb)+" "+
                    "2,"+(this.AMax-this.sb)+" "+
                    "2,"+(this.AMax-2)+" "+
                    (this.sc-2)+","+(this.AMax-2)+" "+
                    (this.sc-2)+","+(this.AMax-this.se+this.HallLength-2)+" "+
                    (this.sd-2)+","+(this.AMax-this.se+this.HallLength-2)+" "+
                    (this.sd-2)+","+(this.fdt+this.HallLength+2)+" ";
        }
    }
  }