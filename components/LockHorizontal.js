// 水平鎖
import HoleRectangle from './HoleRectangle.js';
import HoleIngrained from './HoleIngrained.js';
import HoleIngrainedRectangle from './HoleIngrainedRectangle.js';
import MeasureLabel from './MeasureLabel.js';

export default {
    components: {
        Ml: MeasureLabel,
        HRect: HoleRectangle,
        HIngrained: HoleIngrained,
        HIRect: HoleIngrainedRectangle
    },

    template: `
        <g v-if="lock_method == 'surface'">
            <g  :transform="'translate('+midX+','+yPos+')'">
                <HRect
                    height="20"
                    :width="BoxWidth"
                    :ptY="midPos"
            ></HRect>
            </g>
            <g  :transform="'translate('+(midX-150)+','+yPos+')'">
                <HRect
                    height="20"
                    width="40"
                    :ptY="midPos"
                ></HRect>
            </g>
        </g>

        <g v-if="lock_method == 'ingrained'">
            <g  :transform="'translate('+midX+','+yPos+')'">
                <HIngrained
                    height="29"
                    :width="IngrainedWidth"
                    :ptY="midPos"
                    :type="type"
                ></HIngrained>
            </g>
            <g  :transform="'translate('+(midX-150)+','+yPos+')'">
                <HIRect
                    height="29"
                    width="71"
                    :ptY="midPos"
                ></HIRect>
            </g>
        </g>
        
        <Ml 
            :length="midPos"
            :label="midPos"
            :position=[midX+10,midPos+yPos,-90]>
        </Ml>
        <Ml 
            length="150"
            label="150"
            :position=[midX-150,midPos+yPos+40,0]>
        </Ml>
    `,

    props: {
        midX: {
            type: Number,
            default: 0
        },
        yPos: {
            type: Number,
            default: 0
        },
        midPos: {
            type: Number,
            default: 0
        },
        lock_method: {
            type: String,
            default: 0
        },
        type: {
            type: String,
            default: ''
        }
    },

    computed: {
        midY() {
            return ( this.fdt-1 ) / 2;
        },

        BoxWidth() {
            if (this.type == "power") {
                return 28;
            }
            return 40;
        },

        IngrainedWidth() {
            if (this.type == "power") {
                return 58;
            }
            return 71;
        },
    }
}