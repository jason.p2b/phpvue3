// CN-26K 框 開孔
// 崁入式未完成

import HoleRectangle from './HoleRectangle.js';
import HoleIngrained from './HoleIngrained.js';
import MeasureLabel from './MeasureLabel.js';

export default {
    components: {
        Ml: MeasureLabel,
        HRect: HoleRectangle,
        HIngrained: HoleIngrained
    },

    template: `
        <g v-if="lock_method == 'surface'">
            <g  :transform="'translate('+(midX+21)+','+yPos+')'">
                <HRect
                    height="20"
                    width="40"
                    :ptY="midPos"
                ></HRect>
            </g>
            <Ml 
                :length="21"
                :label="21"
                :position=[midX,midPos+yPos-7,0]>
            </Ml>
            <Ml 
                :length="20"
                :label="20"
                :position=[(midX+21),midPos+yPos-7,0]>
            </Ml>
            <Ml 
                :length="midPos"
                :label="midPos"
                :position=[midX-10,midPos+yPos,-90]>
            </Ml>
        </g>

        <g v-if="lock_method == 'ingrained'">
            <g  :transform="'translate('+(midX)+','+(midPos+yPos)+')'">
                <text>開孔未設定</text>
            </g>

        </g>
    `,

    props: {
        midX: {
            type: Number,
            default: 0
        },
        yPos: {
            type: Number,
            default: 0
        },
        midPos: {
            type: Number,
            default: 0
        },
        lock_method: {
            type: String,
            default: 0
        }
    },

}