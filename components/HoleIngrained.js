// 崁入式 門框開孔圖
import MeasureLabel from './MeasureLabel.js';

export default {
    components: {
        Ml: MeasureLabel
    },

    template: `
        <polyline :points="ingrainedShape" class="svgShape" />
        <circle :cx="-width/2-16.5" :cy="ptY-5" r="3" class="svgShape" />
        <circle :cx="-width/2-16.5" :cy="ptY-5" r="2" class="svgShape" />
        <circle :cx="-width/2-16.5" :cy="ptY+5" r="3" class="svgShape" />
        <circle :cx="-width/2-16.5" :cy="ptY+5" r="2" class="svgShape" />
        <circle :cx="width/2+16.5" :cy="ptY-5" r="3" class="svgShape" />
        <circle :cx="width/2+16.5" :cy="ptY-5" r="2" class="svgShape" />
        <circle :cx="width/2+16.5" :cy="ptY+5" r="3" class="svgShape" />
        <circle :cx="width/2+16.5" :cy="ptY+5" r="2" class="svgShape" />
        <Ml 
            :length="width"
            :label="width"
            :position=[-width/2,ptY+height/1+3,0]>
        </Ml>
        <Ml 
            :length="height"
            :label="height"
            :position=[width/1+10,ptY+height/2,-90]>
        </Ml>
        <Ml v-if="type != 'power'"
            length="15.5"
            label="15.5"
            :position=[-width/2,ptY-height/2,0]>
        </Ml>
        <Ml 
            length="16.5"
            label="16.5"
            :position=[width/2,ptY-height/2,0]>
        </Ml>
        <Ml 
            length="10"
            label="10"
            :position=[width/2+16.5,ptY+5,-90]>
        </Ml>
    `,

    props: {
        height: {
            type: Number,
            default: 0
        },
        width: {
            type: Number,
            default: 0
        },
        ptY: {
            type: Number,
            default: 0
        },
        type: {
            type: String,
            default: ''
        }
    },

    computed: {
        ingrainedShape() {
            let shape = (-this.width/2) + "," + (this.ptY-this.height/2) + " " +
                        (-this.width/2) + "," + (this.ptY+this.height/2) + " " +
                        (this.width/2) + "," + (this.ptY+this.height/2) + " " +
                        (this.width/2) + "," + (this.ptY-this.height/2) + " ";

            if (this.type == "power") {
                return  (-this.width/2) + ",0 " +
                        shape +
                        (this.width/2) + ",0 ";
            } 
            
            return  (-this.width/2+15.5) + ",0 " +
                    (-this.width/2+15.5) + "," + (this.ptY-this.height/2) + " " +
                    shape +
                    (this.width/2-15.5) + "," + (this.ptY-this.height/2) + " " +
                    (this.width/2-15.5) + ",0";
        }
    }
}