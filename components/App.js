import InputGroup from './InputGroup.js';
import Outline from './Outline.js';
import FrameCutView from './FrameCutView.js';
import FrameVerticalView from './FrameVerticalView.js';
import MeasureLabel from './MeasureLabel.js';
import Lock from './Lock.js';
import Hinge from './Hinge.js';

export default {
    components: {
      Ig: InputGroup,
      Outline : Outline,
      Fcv : FrameCutView,
      Fvv : FrameVerticalView,
      Ml : MeasureLabel,
      Lock : Lock,
      Hinge : Hinge,
    },
    data() {
      return {
        'frame_type': '60A',
        'frame_shape': 'HA',
        'fA': 230,
        'fB': 30,
        'fC': 55,
        'fD': 77,
        'fE': 56,
        'fdt': 61,
        'interior_panel': 9,
        'exterior_panel': 6,
        'fhC': 45,
        'fhD': 68,
        'max':150,
        'depth':20,
        'height': 2120,
        'width': 1000,
        'gap': 17,
        'lock': 'DA61Q',
        'lock_method': 'ingrained',
        'lock_height': 1050,
        'lock_height_origin': 'top',
        'hinge': '',
        'forty_five': false,
        'cutX': "40",
        'verticalX': "180",
      }
    },

    computed: {
      AMax() {
        if (this.fA > this.max) {
          return this.max
        }
        return this.fA
      },
      HingePos() {
        let y = this.AMax+125;
        return "translate("+this.cutX+","+ y +") scale(1,-1)";
      },
      HingeVerticalPos() {
        let y = this.AMax+125;
        return "translate(" + this.verticalX + "," + y +") scale(1,-1)";
      },
      LockPos() {
        let y = this.AMax+185;
        return "translate("+this.cutX+","+ y +")";
      },
      LockVerticalPos() {
        let y = this.AMax+185;
        return "translate(" + this.verticalX + ","+ y +")";
      }
    },

    watch: {
      'frame_shape': function () {
        if (this.frame_shape.includes('L')) {
          this.fE = 0;
        }
      },
      'interior_panel': function () {
        this.fdt = 46 + parseInt(this.interior_panel) + parseInt(this.exterior_panel);
        
      },
      'exterior_panel': function () {
        this.fdt = 46 + parseInt(this.interior_panel) + parseInt(this.exterior_panel);
      },
      'frame_type': function () {
        if(this.frame_type.includes('四面框')) {
          this.depth = this.fhC;
          this.gap = 5;
        }
      },
      'fhC': function () {
        if(this.frame_type.includes('四面框')) {
          this.depth = this.fhC;
        }
      }
    }
  }