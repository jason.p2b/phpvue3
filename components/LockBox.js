// 水泥盒
import MeasureLabel from './MeasureLabel.js';

export default {
    components: {
        Ml: MeasureLabel
    },

    template: `
        <rect :x="midX - 75" :y="midPos + yPos - 15" height="30" width="150" class="svgShape" />
        <text :transform="textPos">水泥盒</text>
    `,

    props: {
        midX: {
            type: Number,
            default: 0
        },
        yPos: {
            type: Number,
            default: 0
        },
        midPos: {
            type: Number,
            default: 0
        },
    },

    computed: {
        textPos() {
            return "translate(" + (this.midX-25) + "," + (this.midY+7) + ")";
        }
    }
}