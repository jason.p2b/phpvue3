<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Vue.js in PHP</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
  <style>
    .blue {color: blue;}
    .red {color: red;}
    .green{color:green;}

  .svgShape {
    stroke: #000;
    fill: none;
  }
  .svgText {
    fill:#f00;
    text-anchor:middle;
    font-size: 9px;
  }
  .svgTextBackground {
    stroke-width: 2; stroke:#fff; fill:#fff;
  }
  .svgMeasure {
    stroke:#3c3; 
    fill:none;
  }
  

  </style>
</head>

<body>
<div id="app">
<div class="row">
  <div class="col-2 accordion">

  <div class="accordion-item">
      <h3 class="accordion-header" id="acc-frame-heading">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#acc-frame">
          基本尺寸
        </button>
      </h3>
      <div class="accordion-collapse collapse show" id="acc-frame">
        <div id="accordion-body">
          <Ig type="number" v-model="height">高度</Ig>
          <Ig type="number" v-model="width">寬度</Ig>
          <Ig type="number" v-model="depth" :disabled="frame_type=='四面框' ? 1 : 0">埋入</Ig>
          <Ig type="number" v-model="gap" :disabled="frame_type=='四面框' ? 1 : 0">下門縫</Ig>
          <div class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text" for="frame_type">類別</div>
            </div>
            <select name="frame_type" id="frame_type" v-model="frame_type">
              <option value="60A">60A</option>
              <option value="四面框">四面框</option>
              <option value="玄關框">玄關框</option>
            </select>
          </div>
        </div>
      </div>
    </div>

    <div class="accordion-item">
      <h3 class="accordion-header" id="acc-frameCut-heading">
        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#acc-frameCut">
          框型
        </button>
      </h3>
      <div class="accordion-collapse collapse" id="acc-frameCut">
        <div id="accordion-body">
          <div class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text" for="frame_shape">框形</div>
            </div>
            <select name="frame_shape" id="frame_shape" v-model="frame_shape">
              <option value="LI">L型框</option>
              <option value="LA">L型氣密框</option>
              <option value="LF">L型壓扁氣密框</option>
              <option value="TI">凸字型框</option>
              <option value="TA">凸字型氣密框</option>
              <option value="TF">凸字型壓扁氣密框</option>
              <option value="HA">玄關框</option>
            </select>
          </div>
          <Ig type="number" v-model="fA">框深A</Ig>
          <Ig type="number" v-model="fB">後折B</Ig>
          <Ig type="number" v-model="fC">框厚C</Ig>
          <Ig type="number" v-model="fD">框厚D</Ig>
          <Ig v-show="!frame_shape.includes('L')" type="number" v-model="fE" >外玄關</Ig>
          <Ig type="number" v-model="fdt" :disabled="frame_shape.includes('H') ? 1 : 0">框門厚</Ig>
          <div v-show="frame_shape.includes('H')" class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text" for="exterior_panel">外室材</div>
            </div>
            <select name="exterior_panel" id="exterior_panel" v-model="exterior_panel">
              <option value="0">無</option>
              <option value="1">0.8mm 壓花板</option>
              <option value="6">9mm 鑄鋁板</option>
              <option value="9">12mm 木板</option>
            </select>
          </div>
          <div v-show="frame_shape.includes('H')" class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text" for="interior_panel">內室材</div>
            </div>
            <select name="interior_panel" id="interior_panel" v-model="interior_panel">
              <option value="0">無</option>
              <option value="1">0.8mm 壓花板</option>
              <option value="6">9mm 木板</option>
              <option value="9">12mm 木板</option>
            </select>
          </div>
          <Ig type="number" v-model="fhC">橫料框厚C</Ig>
          <Ig type="number" v-model="fhD">橫料框厚D</Ig>
        </div>
      </div>
    </div>

    <div class="accordion-item" id="svg-accordion">
      <h3 class="accordion-header" id="acc-lock-heading">
        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#acc-lock">
          門鎖
        </button>
      </h3>
      <div class="accordion-collapse collapse" id="acc-lock">
        <div id="accordion-body">
          <div class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text" for="lock">門鎖</div>
            </div>
            <select name="lock" id="lock" v-model="lock">
              <option value=""> 無 </option>
              <optgroup label="平推">
                <option value="CN-1600">CN-1600 平推鎖</option>
                <option value="F6038">F6038 平推鎖</option>
                <option value="916A">Power 916A 平推鎖</option>
              </optgroup>
              <optgroup label="單點">
                <option value="CN-607">CN-607 平頭鎖</option>
                <option value="CN-608">CN-608 平頭鎖</option>
                <option value="DA61Q">DA61Q 單點鎖</option>
                <option value="Power90">Power 90 單點鎖</option>
              </optgroup>
              <optgroup label="水平">
                <option value="CN-21-01S">CN-21-01S 水平鎖</option>
                <option value="CN-26K">CN-26K 水平鎖</option>
                <option value="LF603T+DA61Y">LF603T + DA61Y 水平鎖</option>
                <option value="9014">Power 9014 水平鎖</option>
                <option value="J5115">Power J5115匣式水平鎖</option>
              </optgroup>
              <optgroup label="電子">

              </optgroup>
              <optgroup label="其他">
                
              </optgroup>
            </select>
          </div>
          <div class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text" for="lock_method">開孔方式</div>
            </div>
            <select name="lock_method" id="lock_method" v-model="lock_method">
              <option value="box">水泥盒</option>
              <option value="surface">表面式</option>
              <option value="ingrained">崁入式</option>
              <option value="none">不開</option>
            </select>
          </div>
          <Ig type="number" v-model="lock_height">鎖高</Ig>
          <div class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text" for="lock_height_origin">鎖高起點</div>
            </div>
            <select name="lock_height_origin" id="lock_height_origin" v-model="lock_height_origin">
              <option value="top">從門扇 上緣 </option>
              <option value="bottom">從門扇 下緣</option>
              <option value="floor">從地坪線</option>
            </select>
          </div>
        </div>
      </div>
    </div>

    <div class="accordion-item">
      <h3 class="accordion-header" id="acc-hinge-heading">
        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#acc-hinge">
          鉸鍊
        </button>
      </h3>
      <div class="accordion-collapse collapse" id="acc-hinge">
        <div id="accordion-body">
          <div class="input-group mb-2">
              <div class="input-group-prepend">
                <div class="input-group-text" for="hinge">門鎖</div>
              </div>
              <select name="hinge" id="hinge" v-model="hinge">
                <option value=""> 無 </option>
                <option value="box">不開孔 只補強</option>
                <optgroup label="上下鉸鏈">
                  <option value="GD-902">正東 GD-902</option>
                  <option value="CN-240">正東 CN-240</option>
                  <option value="OK-901">俐昌 OK-901</option>
                  <option value="7125">毅欣 7125</option>
                </optgroup>
                <optgroup label="地鉸鏈">
                  <option value="CN-180">正東 CN-180</option>
                  <option value="750">加安 750</option>
                  <option value="Power-1300">東鐵 Power-1300</option>
                </optgroup>
                <optgroup label="旗型鉸鏈">
                  <option value="CN-1050A">正東 CN-1050A 五孔</option>
                  <option value="OK-601">俐昌 OK-601 五孔</option>
                  <option value="CN-1050L">正東 CN-1050L 六孔</option>
                  <option value="Power-106">東鐵 Power 106 六孔</option>
                  <option value="Power-104">東鐵 Power 104 四孔</option>
                </optgroup>
                <optgroup label="重型鉸鏈">
                  <option value="CN-127">正東 CN-127</option>
                </optgroup>
                <optgroup label="其他">
                  
                </optgroup>
              </select>
            </div>
          </div>
      </div>
    </div>

    <div class="accordion-item">
      <h3 class="accordion-header" id="acc-hardware-heading">
        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#acc-hardware">
          其他五金
        </button>
      </h3>
      <div class="accordion-collapse collapse" id="acc-hardware">
        <div id="accordion-body">
          <Ig type="number" v-model="fhD">橫料框厚D</Ig>
        </div>
      </div>
    </div>
  </div>

  <div class="col-10">
  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1200 835" style="font-family:Arial;"> -->
    
    <Outline></Outline>

    <g id="hinge-frame-cut" :transform="HingePos">
      <Fcv 
        :shape="frame_shape"
        :sa="fA"
        :sb="fB"
        :sc="fC"
        :sd="fD"
        :se="fE"
        :fdt="fdt"
        flip=true
      ></Fcv>
    </g>

    <g id="hinge-frame-cut" :transform="HingeVerticalPos">
      <Fvv 
        :frame_shape="frame_shape"
        :sa="fA"
        :sb="fB"
        :sc="fhC"
        :sd="fhD"
        :se="fE"
        :fdt="fdt"
        :height="height"
        flip=true
      ></Fvv>
      
      <Hinge
        :frame_type="frame_type"
        :hinge="hinge"
        :sc="fhC"
        :fdt="fdt"
        :gap="gap"
        :depth="depth"
        :height="height"
        flip=true
      ></Hinge>

    </g>

    <g id="lock-frame-cut" :transform="LockPos">
      <Fcv 
        :shape="frame_shape"
        :sa="fA"
        :sb="fB"
        :sc="fC"
        :sd="fD"
        :se="fE"
        :fdt="fdt"
      ></Fcv>
    </g>

    <g id="lock-frame-vertical" :transform="LockVerticalPos">
      <Fvv
        :frame_shape="frame_shape"
        :sa="fA"
        :sb="fB"
        :sc="fhC"
        :sd="fhD"
        :se="fE"
        :fdt="fdt"
        :height="height"
      ></Fvv>
      <Lock
        :frame_type="frame_type"
        :frame_shape="frame_shape"
        :exterior_panel="exterior_panel"
        :lock="lock"
        :lock_height="lock_height"
        :lock_method="lock_method"
        :lock_height_origin="lock_height_origin"
        :sc="fhC"
        :fdt="fdt"
        :gap="gap"
        :depth="depth"
        :height="height"
      ></Lock>
    </g>

    <g id="lock-frame-cut" transform="translate(40,635)">
    <Fcv 
        :shape="frame_shape"
        :sa="fA-4"
        :sb="fB"
        :sc="fhC"
        :sd="fhD"
        :se="fE"
        :fdt="fdt-2"
      ></Fcv>
    </g>

  </svg>
  </div>
</div>
  
</div>

<script type="module">
  import App from "./components/App.js";  
  Vue.createApp(App).mount('#app');
</script>
</script>
</body>
</html>